<?php
namespace Space10\UrlRewrite;

use Zend\EventManager\EventInterface;
use Zend\ModuleManager\Feature\BootstrapListenerInterface;
use Zend\ModuleManager\Feature\ConfigProviderInterface;
use Zend\Mvc\ModuleRouteListener;

/**
 */
class Module implements BootstrapListenerInterface, ConfigProviderInterface
{

    /**
     * (non-PHPdoc)
     * @see  \Zend\ModuleManager\Feature\BootstrapListenerInterface::onBootstrap()
     *
     * @param EventInterface $event
     *
     * @return array|void
     */
    public function onBootstrap(EventInterface $event)
    {
        /* @var $app \Zend\Mvc\Application */
        $app = $event->getApplication();
        $eventManager = $app->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $listener = $app->getServiceManager()->get('Space10\UrlRewrite\Listener\RouteRewriteListener');
        $moduleRouteListener->attach($eventManager);
        $eventManager->attachAggregate($listener);

    }

    /**
     * (non-PHPdoc)
     * @see \Zend\ModuleManager\Feature\ConfigProviderInterface::getConfig()
     */
    public function getConfig()
    {
        return include __DIR__ . DIRECTORY_SEPARATOR . 'config' . DIRECTORY_SEPARATOR . 'module.config.php';
    }

}
