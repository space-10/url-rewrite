<?php
namespace Space10Test\UrlRewrite\Listener;

use Space10\UrlRewrite\Entity\UrlRewrite;
use Space10\UrlRewrite\Listener\RouteRewriteListener;
use Zend\Mvc\MvcEvent;

class RouteRewriteListenerTest extends \PHPUnit_Framework_TestCase
{
    protected function getRewrites() {

        $rw = new UrlRewrite();
        $rw->setId(1)->setDescription('foo bar')->setIdPath('foobarbaz')->setRequestPath('/foo-bar')->setTargetPath('/foo-baz');
        return [$rw];
    }

    public function testListenerAttach()
    {
        $emMock = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $serviceLocatorMock = $this->getMockBuilder('Zend\ServiceManager\ServiceLocatorInterface')->getMock();

        $listener = new RouteRewriteListener([], $serviceLocatorMock, $emMock);

        $eventMock = $this->getMockBuilder('Zend\EventManager\EventManagerInterface')->getMock();
        $eventMock->expects($this->once())->method('attach')->with(MvcEvent::EVENT_ROUTE, [
            $listener,
            'onRoute'
        ], 100)->willReturn(spl_object_hash($listener));

        $listener->attach($eventMock);
    }

    public function testRouting()
    {
        $rewrites = $this->getRewrites();
        $repositoryMock = $this->getMockBuilder('Doctrine\Common\Persistence\ObjectRepository')->disableOriginalConstructor()->getMock();
        $repositoryMock->expects($this->once())->method('findAll')->willReturn($rewrites);

        $emMock = $this->getMockBuilder('Doctrine\ORM\EntityManager')->disableOriginalConstructor()->getMock();
        $emMock->expects($this->atLeastOnce())->method('getRepository')->with($this->equalTo('Space10\UrlRewrite\Entity\UrlRewrite'))->willReturn($repositoryMock);

        $controllerMock = $this->getMockBuilder('Space10\UrlRewrite\Controller\RewriteControllerInterface')->getMock();
        $serviceLocatorMock = $this->getMockBuilder('Zend\ServiceManager\ServiceLocatorInterface')->getMock();
        $serviceLocatorMock->expects($this->once())->method('has')->with(RouteRewriteListener::DEFAULT_REWRITE_CONTROLLER)->willReturn(true);
        $serviceLocatorMock->expects($this->once())->method('get')->with(RouteRewriteListener::DEFAULT_REWRITE_CONTROLLER)->willReturn($controllerMock);

        $listener = new RouteRewriteListener([], $serviceLocatorMock, $emMock);

        $routerMock = $this->getMockBuilder('Zend\Mvc\Router\RouteStackInterface')->disableOriginalConstructor()->getMock();
        $routerMock->expects($this->exactly(count($rewrites)))->method('addRoute');


        $eventMock = $this->getMockBuilder('Zend\Mvc\MvcEvent')->disableOriginalConstructor()->getMock();
        $eventMock->expects($this->once())->method('getRequest')->willReturn($this->getRewrites());
        $eventMock->expects($this->once())->method('getRouter')->willReturn($routerMock);

        $listener->onRoute($eventMock);
    }
}