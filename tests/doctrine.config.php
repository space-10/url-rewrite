<?php
return [
    'doctrine'                => [
        'connection' => [
            // default connection name
            'orm_default' => [
                'driverClass' => 'Doctrine\DBAL\Driver\PDOSqlite\Driver',
                'params'      => [
                    'charset' => 'utf8',
                    'path' => __DIR__.'/Space10Test/UrlRewrite/_files/test.db',
                ]
            ]
        ]
    ],
];