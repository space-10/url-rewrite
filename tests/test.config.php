<?php
return [
    'modules'                 => [
        'DoctrineModule',
        'DoctrineORMModule',
        'Space10\Di',
        'Space10\Admin',
        'Space10\UrlRewrite'
    ],
    'module_listener_options' => [
        'config_glob_paths' => [
            __DIR__ . '/doctrine.config.php'
        ],
        'module_paths' => [
            'module',
            'vendor',
        ],
    ],
];
