<?php
namespace Space10\UrlRewrite;

return [
    'doctrine'     => include __DIR__ . DIRECTORY_SEPARATOR . 'doctrine.config.php',
    'controllers'  => [
        'invokables' => [
            //'Space10\UrlRewrite\Admin\Controller\UrlRewrite' => 'Space10\UrlRewrite\Admin\Controller\UrlRewriteController',
            //'Space10\UrlRewrite\Controller\Rewrite'          => 'Space10\UrlRewrite\Controller\RewriteController',
        ],
    ],
    'space10urlrewrite' => [
        'rewrite_controller' => 'Space10\UrlRewrite\Controller\RewriteController'
    ],
    'router'       => [
        'routes' => [
            'admin' => [
                'child_routes' => [
                    'urlrewrite' => [
                        'type'    => 'Segment',
                        'options' => [
                            'route'       => '/urlrewrite[/]',
                            'constraints' => [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*'
                            ],
                            'defaults'    => [
                                'controller' => 'Space10\UrlRewrite\Admin\Controller\UrlRewrite',
                                'action'     => 'index'
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ],
    'view_manager' => [
        'template_path_stack' => [
            __NAMESPACE__ => __DIR__ . '/../view'
        ]
    ]
];