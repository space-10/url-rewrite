<?php
namespace Space10\UrlRewrite\Listener;

use Doctrine\Common\Persistence\ObjectManager;
use Space10\UrlRewrite\Controller\RewriteControllerInterface;
use Space10\UrlRewrite\Exception;
use Zend\Console\Request;
use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\Exception\RuntimeException;
use Zend\Mvc\Router\Http\Literal;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class RouteRewriteListener
 * @package Space10\UrlRewrite\Listener
 */
class RouteRewriteListener extends AbstractListenerAggregate
{

    const DEFAULT_REWRITE_CONTROLLER = 'Space10\UrlRewrite\Controller\RewriteController';
    const DEFAULT_REWRITE_ACTION = 'route';

    /**
     * @var array
     */
    protected $config;

    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @param array                   $config
     * @param ServiceLocatorInterface $serviceLocator
     * @param ObjectManager           $objectManager
     */
    public function __construct(array $config, ServiceLocatorInterface $serviceLocator, ObjectManager $objectManager)
    {
        $this->config = $config;
        $this->objectManager = $objectManager;
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * Attach one or more listeners
     *
     * Implementors may add an optional $priority argument; the EventManager
     * implementation will pass this to the aggregate.
     *
     * @param EventManagerInterface $events
     *
     * @return void
     */
    public function attach(EventManagerInterface $events)
    {

        $this->listeners[] = $events->attach(MvcEvent::EVENT_ROUTE, [
            $this,
            'onRoute'
        ], 100);
    }

    /**
     *
     * @param MvcEvent $e
     *
     * @throws \Exception
     */
    public function onRoute(MvcEvent $e)
    {
        if ($e->getRequest() instanceof Request) {
            return;
        }

        $defaultController = static::DEFAULT_REWRITE_CONTROLLER;
        if (isset($this->config['space10urlrewrite']) && isset($this->config['space10urlrewrite']['rewrite_controller'])) {
            $defaultController = $this->config['space10urlrewrite']['rewrite_controller'];
        }

        if (!$this->serviceLocator->has($defaultController)) {
            throw new Exception\ControllerNotFoundException('Controller "' . $defaultController . '" not found in service locator.');
        } elseif (!$this->serviceLocator->get($defaultController) instanceof RewriteControllerInterface) {
            throw new Exception\InvalidControllerException('Rewrite controller must be an instance of \Space10\UrlRewrite\Controller\RewriteControllerInterface');
        }

        $repository = $this->objectManager->getRepository('Space10\UrlRewrite\Entity\UrlRewrite');
        $rewrites = $repository->findAll();

        $routeOptions = [
            'controller' => $defaultController,
            'action'     => static::DEFAULT_REWRITE_ACTION
        ];

        $router = $e->getRouter();
        foreach ($rewrites as $rewrite) {
            /* @var $rewrite \Space10\UrlRewrite\Entity\UrlRewrite */

            $idPath = str_replace('/', '-', $rewrite->getIdPath());
            $route = new Literal($rewrite->getRequestPath(), $routeOptions);
            try {
                $router->addRoute($idPath, $route);
            } catch (RuntimeException $ex) {
                throw new Exception\RoutingException('Error while adding route "' . $idPath . '": ' . var_export($route,
                        true),
                    0, $ex);
            }
        }
    }
}
