<?php
namespace Space10\UrlRewrite\Admin\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use DoctrineModule\Persistence\ObjectManagerAwareInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Space10\UrlRewrite;

/**
 * Class UrlRewriteController
 * @package Space10\UrlRewrite\Admin\Controller
 */
class UrlRewriteController extends AbstractActionController implements ObjectManagerAwareInterface
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * Default action if none provided
     *
     * @return array
     */
    public function indexAction()
    {

        $repository = $this->getObjectManager()->getRepository('Space10\UrlRewrite\Entity\UrlRewrite');
        $rewrites = $repository->findAll();

        $form = new UrlRewrite\Admin\Form\UrlRewrite();

        $entity = new UrlRewrite\Entity\UrlRewrite();
        $form->bind($entity);

        /* @var $request \Zend\Http\PhpEnvironment\Request */
        $request = $this->getRequest();

        if ($request->isPost())
        {

            $form->setData($request->getPost());
            $form->setBindOnValidate(UrlRewrite\Admin\Form\UrlRewrite::BIND_MANUAL);

            if ($form->isValid())
            {
                // Redirect to list of albums
                return $this->redirect()->toRoute('admin/urlrewrite');
            }
        }
        return new ViewModel([
            'form' => $form,
            'rewrites' => $rewrites
        ]);
    }

    /**
     * Set the object manager
     *
     * @param ObjectManager $objectManager
     *
     * @return $this
     */
    public function setObjectManager(ObjectManager $objectManager)
    {
        $this->objectManager = $objectManager;
        return $this;
    }

    /**
     * Get the object manager
     *
     * @return ObjectManager
     */
    public function getObjectManager()
    {
        return $this->objectManager;
    }
}
