<?php
namespace Space10\UrlRewrite\Admin\Form;

use Zend\Form\Element;
use Zend\Form\Form;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;

/**
 * Class UrlRewrite
 * @package Space10\UrlRewrite\Admin\Form
 */
class UrlRewrite extends Form
{

    /**
     * @param  null|int|string  $name    Optional name for the element
     * @param  array            $options Optional options for the element
     */
    public function __construct($name = null, array $options = [])
    {

        parent::__construct($name ? $name : 'url_rewrite', $options);
        $this->setAttribute('method', 'POST');

        $csrf = new Element\Csrf('token');
        $this->add($csrf);

        $element = new Element\Text('request_path');
        $element->setLabel(__('Request path'));
        $element->setAttribute('required', true);
        $this->add($element);

        $element = new Element\Text('target_path');
        $element->setLabel(__('Target path'));
        $element->setAttribute('required', true);
        $this->add($element);

        $element = new Element\Select('redirect');
        $element->setValueOptions([
            0 => __('No'),
            [
                'label'      => __('Yes'),
                'value'      => 1,
                'attributes' => [
                    'data-show-element' => 'options'
                ]
            ],
        ]);
        $element->setLabel(__('Redirect'));
        $this->add($element);

        $element = new Element\Select('options');
        $element->setValueOptions([
            'MP' => __('Moved Permanently'),
            'RT' => __('Temporary Redirect'),
            'RP' => __('Permanent Redirect')
        ]);
        $element->setLabel(__('Options'));
        $this->add($element);

        $inputFilter = new InputFilter();
        $factory = new InputFactory();

        $inputFilter->add($factory->createInput($this->getFilterSpecification()));
        $this->setInputFilter($inputFilter);
    }

    /**
     * @return array
     */
    private function getFilterSpecification()
    {

        return [
            'name'       => 'request_path',
            'required'   => true,
            'filters'    => [
                [
                    'name' => 'StripTags'
                ],
                [
                    'name' => 'StringTrim'
                ]
            ],
            'validators' => [
                [
                    'name'    => 'EmailAddress',
                    'options' => [
                        'encoding' => 'UTF-8',
                        'min'      => 3,
                        'max'      => 100
                    ]
                ]
            ]
        ];
    }
}
