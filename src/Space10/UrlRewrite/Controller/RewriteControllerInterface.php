<?php
namespace Space10\UrlRewrite\Controller;


/**
 * Interface RewriteControllerInterface
 * @package Space10\UrlRewrite\Controller
 */
interface RewriteControllerInterface {

    /**
     * @return mixed
     */
    public function routeAction();
}