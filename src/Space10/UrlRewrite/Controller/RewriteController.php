<?php
namespace Space10\UrlRewrite\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Zend\Http\Response;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\Mvc\Router\RouteStackInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

/**
 * Class RewriteController
 * @package Space10\UrlRewrite\Controller
 */
class RewriteController extends AbstractActionController implements RewriteControllerInterface
{
    const STATUS_CODE_308 = 308;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var RouteStackInterface
     */
    protected $router;

    /**
     * @var ServiceLocatorInterface
     */
    protected $controllerLoader;

    /**
     * @param ObjectManager $entityManager
     * @param               $router
     * @param               $controllerLoader
     */
    public function __construct(
        RouteStackInterface $router,
        ServiceLocatorInterface $controllerLoader,
        ObjectManager $entityManager
    ) {
        $this->objectManager = $entityManager;
        $this->router = $router;
        $this->controllerLoader = $controllerLoader;
    }

    /**
     * @return array|mixed
     * @todo check browser compatibility to http status code 308
     */
    public function routeAction()
    {
        $uri = $this->getRequest()->getUri();

        $repository = $this->objectManager->getRepository('Space10\UrlRewrite\Entity\UrlRewrite');
        $rewrites = $repository->findBy([
            'requestPath' => $uri->getPath()
        ]);

        $newReq = clone ($this->getRequest());
        foreach ($rewrites as $entity) {
            /* @var $entity \Space10\UrlRewrite\Entity\UrlRewrite */
            // set new url path
            $targetPath = $entity->getTargetPath();

            $uri = $newReq->getUri();
            $uri->setPath($targetPath);
            $newReq->setUri($uri);

            // search for matching target route
            $routeMatch = $this->router->match($newReq);
            if (!$routeMatch) {
                return $this->notFoundAction();
            }

            // return early on redirects
            $options = $entity->getOptions();
            if ($options == 'RP' || $options == 'RT' || $options == 'MP') {
                if ($options == 'MP') { // Moved Permanent
                    $statusCode = Response::STATUS_CODE_301;
                } elseif ($options == 'RT') { // Temporary Redirect
                    $statusCode = Response::STATUS_CODE_307;
                } elseif ($options == 'RT') {
                    $statusCode = static::STATUS_CODE_308;
                }

                $response = $this->getResponse();
                $response->getHeaders()->addHeaderLine('Location', $newReq->getUri()->toString());
                $response->setStatusCode($statusCode);
                return $response;
            }

            $controllerName = $routeMatch->getParam('controller', 'not-found');

            if (!$this->controllerLoader->has($controllerName)) {
                return $this->notFoundAction();
            }

            $params = [
                'action' => 'index'
            ];

            // fake event data
            $this->getEvent()->setRouteMatch($routeMatch);
            $this->getEvent()->setController($controllerName);
            $this->getEvent()->setParams(array_merge($this->getEvent()->getParams(), $params));

            // dispatch the forward
            return $this->forward()->dispatch($controllerName, $params);
        }
        return $this->notFoundAction();
    }
}

