<?php

namespace Space10\UrlRewrite\Exception;


/**
 * Class InvalidControllerException
 * @package Space10\UrlRewrite\Exception
 */
class InvalidControllerException extends \InvalidArgumentException implements ExceptionInterface {

}