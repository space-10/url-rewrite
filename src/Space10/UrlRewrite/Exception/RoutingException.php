<?php

namespace Space10\UrlRewrite\Exception;


/**
 * Class RoutingException
 * @package Space10\UrlRewrite\Exception
 */
class RoutingException extends \RuntimeException implements ExceptionInterface {

}