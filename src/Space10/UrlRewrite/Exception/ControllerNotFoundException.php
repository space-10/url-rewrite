<?php
namespace Space10\UrlRewrite\Exception;


/**
 * Class ControllerNotFoundException
 * @package Space10\UrlRewrite\Exception
 */
class ControllerNotFoundException extends \InvalidArgumentException implements ExceptionInterface {

}