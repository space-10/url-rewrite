<?php
namespace Space10\UrlRewrite\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="url_rewrite",indexes={@ORM\Index(name="IDX_URL_REWRITE_PATH", columns={"request_path", "target_path"})})
 * @todo create entity interface
 */
class UrlRewrite
{

    /**
     * @ORM\Id
     * @ORM\Column(name="url_rewrite_id", type="integer", nullable=false, options={"unsigned":"true"})
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var integer
     */
    protected $id;

    /**
     * @ORM\Column(name="id_path", type="string", nullable=false, unique=true, options={"comment":"Name of the route"})
     *
     * @var string
     */
    protected $idPath;

    /**
     * The path of the incoming request
     *
     * @ORM\Column(name="request_path", type="string", length=2000, nullable=false, unique=true)
     *
     * @see http://www.boutell.com/newfaq/misc/urllength.html
     * @var string
     */
    protected $requestPath;

    /**
     * @ORM\Column(name="target_path", type="string", nullable=false)
     *
     * @var string
     */
    protected $targetPath;

    /**
     * @ORM\Column(name="is_system", type="boolean", nullable=false, options={"default": 0})
     *
     * @var boolean
     */
    protected $system;

    /**
     * @ORM\Column(name="options", type="string", nullable=false)
     *
     * @var string
     */
    protected $options;

    /**
     * @ORM\Column(name="description", type="text", nullable=false)
     *
     * @var string
     */
    protected $description;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return UrlRewrite
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdPath()
    {
        return $this->idPath;
    }

    /**
     * @param string $idPath
     *
     * @return UrlRewrite
     */
    public function setIdPath($idPath)
    {
        $this->idPath = $idPath;
        return $this;
    }

    /**
     * @return string
     */
    public function getRequestPath()
    {
        return $this->requestPath;
    }

    /**
     * @param string $requestPath
     *
     * @return UrlRewrite
     */
    public function setRequestPath($requestPath)
    {
        $this->requestPath = $requestPath;
        return $this;
    }

    /**
     * @return string
     */
    public function getTargetPath()
    {
        return $this->targetPath;
    }

    /**
     * @param string $targetPath
     *
     * @return UrlRewrite
     */
    public function setTargetPath($targetPath)
    {
        $this->targetPath = $targetPath;
        return $this;
    }

    /**
     * @return boolean
     */
    public function isSystem()
    {
        return $this->system;
    }

    /**
     * @param boolean $system
     *
     * @return UrlRewrite
     */
    public function setSystem($system)
    {
        $this->system = $system;
        return $this;
    }

    /**
     * @return string
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @param string $options
     *
     * @return UrlRewrite
     */
    public function setOptions($options)
    {
        $this->options = $options;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     *
     * @return UrlRewrite
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

}
